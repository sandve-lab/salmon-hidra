library(tidyverse)
library(DESeq2)
library(BiocParallel)

tbl <- read_tsv("/mnt/project/Transpose/hidra/results/salmon-hidra/batch1/fragment_counts_filtered/filtered_counts.tsv",col_types = "ciiiiiiiiiiii")

cnt <- tbl %>% 
  filter(end-start>95 , end-start<105)

m <- as.matrix(select(cnt, -seqname, -start, -end))
rownames(m) <- paste0(cnt$seqname,":",cnt$start,"-",cnt$end)

m1 = m[,c("RNA_1","RNA_2","DNA_1","DNA_2")]
m2 = m[,c("RNA_3","RNA_4","DNA_3","DNA_4")]


dds <- DESeqDataSetFromMatrix(m1,tibble(assay = as.factor(sub("_.$","",colnames(m1)))), ~assay)
dds <- DESeq(dds,parallel = T,BPPARAM=MulticoreParam(10))
res1 <- results(dds, name="assay_RNA_vs_DNA")

dds <- DESeqDataSetFromMatrix(m2,tibble(assay = as.factor(sub("_.$","",colnames(m2)))), ~assay)
dds <- DESeq(dds,parallel = T,BPPARAM=MulticoreParam(10))
res2 <- results(dds, name="assay_RNA_vs_DNA")

q <- cut(res1$baseMean,breaks =  quantile(res1$baseMean,p=seq(0,1,by = 0.1)),include.lowest = T)
tibble(q, lfc1=res1$log2FoldChange,lfc2=res2$log2FoldChange) %>% 
  group_by(q) %>% summarise(R=cor(lfc1,lfc2)) %>% 
  ggplot(aes(x=q,y=R))+geom_point() + xlab("basemean") + ylab("pearson's R") + 
  ggtitle("log2FoldChange correlation between two sets of two replicates. Fragment length=100bp")


