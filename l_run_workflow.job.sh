#!/bin/bash
#SBATCH --ntasks=1               
#SBATCH --nodes=1               
#SBATCH --job-name=snakemake_atacseq
#SBATCH --output=slurm-snakemake-%j.out
#SBATCH --partition=orion,gpu
#SBATCH --no-requeue # possibly avoid restarting if the node fails

# bash strict mode, i.e. stop script on first error
set -euo pipefail

# need snakemake
module load snakemake

# need samtools
module load SAMtools

# need bedtools
module load BEDTools

# need R but loading it here will interfer with singularity. so only load in the jobs that use R
# module load R/4.0.4

snakemake \
  --profile slurm -j 32 --default-resources nodes=1 \
  --cluster-config l_orion_config.json \
  --jobname {rulename}.snakejob.{jobid}.sh \
  --use-conda \
  --printshellcmds \
  -s workflow/Snakefile \
  get_fragment_sequence \
  DESeq_TSV \
  --configfile config/workflow_config.yaml
#  --keep-going
# to omit a result use "-O", e.g.:
#  -O results/TOBIAS/bias_correction/SF_B_downsample25M_corrected.bw

# Dry-run:
# snakemake -n -j1 --printshellcmds -s workflow/Snakefile --configfile config/workflow_config.yaml

# make conda environments:
# snakemake -j1 --use-conda --create-envs-only -s workflow/Snakefile --configfile config/workflow_config.yaml
