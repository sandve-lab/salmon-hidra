#!/bin/bash
#SBATCH --ntasks=2
#SBATCH --nodes=1
#SBATCH --job-name=fastQC

# bash strict mode, i.e. stop script on first error
set -euo pipefail

DATA_DIR=/net/fs-1/SandveLab/rawdata/2021_Alex_Hidra_MPRA/links
OUT_DIR=fastqc

mkdir $OUT_DIR

singularity exec /cvmfs/singularity.galaxyproject.org/all/fastqc:0.11.9--hdfd78af_1 fastqc \
  $DATA_DIR/H01/H01_EKDL210006715-1a_HKH7WDSX2_L1_1.fq.gz \
  $DATA_DIR/H01/H01_EKDL210006715-1a_HKH7WDSX2_L1_2.fq.gz \
  -o $OUT_DIR -t 2 -d $TMPDIR/$USER
