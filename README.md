(Work in progress! Do not expect everything to work or to make sense. )

# Salmon HiDRA

This repo contains the analysis of the "Salmon HiDRA" experiment, where we investigate the gene regulatory elements in Atlantic salmon liver cells using the HiDRA (High-resolution Dissection of Regulatory Activity) method described in https://www.nature.com/articles/s41467-018-07746-1

## Bioinformatic workflow

Run the snakemake workflow like this:

snakemake --use-conda -s workflow/Snakefile --configfile config/workflow_config.yaml

There are various configs (config/ directory) for different runs and slurm scripts used to run then (l_run_workflow_*)

(It is required that R and the R packeges used are installed.)

The workflow does the following:

 * Trim reads
 * FastQC
 * Map reads
 * Bamstats
 * Count fragments
 * filter fragments (rowSums(m==0)==0 & rowMeans(m)>5)
 * DESeq
 * FIMO

## The batches and preliminary sequencing
 
The first batch (now called batch1) was performed in 2021 by Alex but an error in the fragment length selection resulted in very short fragments. A second batch was therefore generated in 2022 by Tom, selecting for longer fragments.

For each batch a preliminary sequencing round was performed to check the library complexity (the number of unique fragments)

## Overview of the lab protocol

1. Construct ATAC library - Extract cell nuclei from liver tissue and treat with Tn5 to produce fragments from open chromatin. This introduces adapters to the fragments that are used in all subsequent PCR amplification steps.
2. Size selection - Select fragment of preferred size
3. PCR amplification (only performed on batch2)
4. Cloning (ligation?) into vector - (This might be the most limiting step)
5. Transformation/Transfection - Vector is inserted into bacterial host
6. PCR + DNA sequencing (including preliminary sequencing)
7. Transfection into cultured liver cells
8. PCR + RNA sequencing - Transcripts (with polyA tale) from the vectors are sequenced (only ATAC fragments that have the adapters will be sequenced)

## initial QC - checking the library complexity

The purpose is mainly to estimate the library complexity, i.e. how many unique fragments there are in the library. If there are too many unique fragments it will have to be sequenced at a higher depth to get good estimate of the RNA/DNA value. If there are too few fragments then it may not cover all the regions of interrest. 

Also check that the fragments map to open chromatin regions.


 
## QCanalysis

Look at fragments and the DESeq results compared with motifs and some ChIP-seq data. Conclusion is that data can to some extent be used to identify regulatory elements. However, it is biased by fragment length and there are problematic genomic regions that probably should have been removed.

Sharpr analysis did not give any significant results. This could be because of the fragment length bias or problematic regions, but it is also hampered by lack of overlap because the fragments are too short.

## sequence model prediction

Attempt to use machine learning to predict the results given the DESeq results per fragment and genome sequence for each fragment.
The analyseReplicateLFCcor.R gives an idea of how reproducible the results are between replicates. It shows that the fragements with high read count (basemean) correlate much better between replicates.

The prediction/ directory shall contain scripts related to this, e.g. predicted tracks.

# Analysis of existing data from humans

There are two studies that have performed the same (similar) experiment, namely the original HiDRA study and the ATAC-STARR study. The plan is to try to analyse the data from those studies to compare the quality.

All scripts related to this will be stored in the "human_comparison" directory

## getting their data

* HiDRA: GSE104001
* ATAC-STARR:

1. Use the SRA run selector on ncbi to get a list of run accessions
2. Download using SRA-Toolkit (nf-ngsfetch failed for some reason)

## Initial look

Use a single sample (SRR6050484) and run the initialQC markdown

1. Get frgament counts with the snakemake workflow
2. Render markdown..


