#!/bin/bash
#SBATCH --ntasks=1               
#SBATCH --nodes=1               
#SBATCH --job-name=snakemake_HIDRA
#SBATCH --output=slurm-snakemake-%j.out
#SBATCH --no-requeue # possibly avoid restarting if the node fails

# bash strict mode, i.e. stop script on first error
set -euo pipefail

# need conda
module load Anaconda3

# need snakemake
module load snakemake

# need samtools
module load SAMtools

# need R.. However it seems to interfer with singularity so I cant use it..
# module load R/4.0.4

snakemake \
  --profile slurm -j 32 --default-resources nodes=1 \
  --cluster-config l_orion_config.json \
  --jobname {rulename}.snakejob.{jobid}.sh \
  --use-conda \
  --printshellcmds \
  -s workflow/Snakefile \
  mergeFragmentCounts \
  --configfile config/workflow_config_batch1_initialQC.yaml
#  --keep-going
# to omit a result use "-O", e.g.:
#  -O results/TOBIAS/bias_correction/SF_B_downsample25M_corrected.bw

# Dry-run:
# snakemake -n -j1 --printshellcmds -s workflow/Snakefile /mnt/SCRATCH/lagr/salmon-hidra-ICSASG_v2/DESeq_bin/DESeq_res_lengthBin.RDS --configfile config/workflow_config_ICSASG_v2.yaml

# make conda environments:
# snakemake -j1 --use-conda --create-envs-only -s workflow/Snakefile --configfile config/workflow_config.yaml
