args = commandArgs(trailingOnly=TRUE)
if (length(args)<2) 
  stop("At least one output and input directory of fragment counts required!\nUsage: Rscript mergeFragCounts.R outdir indir [indir...]")

suppressPackageStartupMessages({
  library(tidyverse)
})

outdir <- args[1]  
fragCntDirs <- args[-1]
cat("outdir =",outdir,"\n")
cat("indir =",paste(fragCntDirs,collapse = ", "),"\n")

##### DEBUGGING only
# fragCntDirs <- file.path("/mnt/SCRATCH/lagr/salmon-hidra/fragment_counts",c("DNA_1", "DNA_2", "DNA_3", "DNA_4", "DNA_5", "RNA_1", 
#   "RNA_2", "RNA_3", "RNA_4", "RNA_5"))
# outdir <- "/mnt/SCRATCH/lagr/salmon-hidra/fragment_counts_merged"


dir.create(outdir,showWarnings = F)

cntTblFiles <- 
  tibble(fragCntDir=fragCntDirs) %>% 
  mutate(sampleID=basename(fragCntDir)) %>% 
  mutate( cntTblFiles = map(fragCntDir,dir, pattern="\\.tsv$")) %>% 
  unnest( cntTblFiles ) %>%
  mutate( contig=sub(".tsv$","",cntTblFiles)) %>% 
  pivot_wider(id_cols=contig, names_from=sampleID, values_from=cntTblFiles)

cat("Number of samples:",length(fragCntDirs),"\n")
cat("Number of contigs:",nrow(cntTblFiles),"\n")

if(nrow(na.omit(cntTblFiles)) < nrow(cntTblFiles)){
  cat("Warning: not all contigs exist in all samples!\n")
  cntTblFiles <- na.omit(cntTblFiles)
  cat("Using contigs found in all samples:",nrow(cntTblFiles),"\n")
}

for( contig in cntTblFiles$contig){
  cat("Processing",contig,"... ")
  outfile = file.path(outdir,paste0(contig,".RDS"))
  
  if(file.exists(outfile)){ 
    cat("Already processed. \n")
    next
  }
  
  filenames=file.path(fragCntDirs,paste0(contig,".tsv"))
  names(filenames) <- basename(dirname(filenames))
  
  cnts <- 
    lapply(filenames, function(file) read_tsv(file,col_names = c("start","end","count"),col_types = "iii"))
  
  if( any(sapply(cnts,nrow)==0) ){
    cat("Warning! No fragments in contig.\n")
    next
  }

  # Quick fix to avoid duplicate fragment counts from fragmentCount.py
  cnts <- lapply(cnts, function(x){
    x %>% group_by(start,end) %>% summarise(count=sum(count), .groups="drop")  
  })
  
  cnts <- 
    cnts %>% 
    bind_rows(.id="sample") %>% 
    pivot_wider(id_cols=c(start,end),names_from=sample,values_from=count,values_fill=0)
  
  saveRDS(cnts,file = outfile)
  cat("Unique fragments:",nrow(cnts)," Saved to",outfile,"\n")
}

cat("Done\n")
