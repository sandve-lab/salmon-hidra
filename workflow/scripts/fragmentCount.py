import argparse
import os
import sys
import pysam
import pandas as pd
import itertools
import logging

MAX_FRAG_LEN = 1000 # Not a filter, fragment longer than this may be discarded

# Function to split an iterable into a series of smaller chunks.
# Returns a generator that yields chunks of the iterable, each containing n elements.
def grouper_it(n, iterable):
    it = iter(iterable)
    while True:
        chunk_it = itertools.islice(it, n)
        try:
            first_el = next(chunk_it)
        except StopIteration:
            return
        yield itertools.chain((first_el,), chunk_it)


def countFragmentsChunk(df):
    
    n = len(df)
    if( n == 0 ):
      logging.warning('Empty chunk!..')
      # return empty results
      return( (pd.DataFrame(columns=['start','end','Count'],dtype='int'), df) )
      
    startPos = min(df.start)
    lastPos = max(df.start)
    
    # filter
    df = df[df['flag'] & 0x800==0] # drop supplementary reads
    nSupp = n-len(df)
    df = df[df['flag'] & 0x2==0x2] # keep only properly paired reads
    nNotProper = n-nSupp-len(df)
    
    readCopyNumber = df['read'].map(df['read'].value_counts()) # count number of times each read ID is observed
    
    # Keep single reads as their mate may be in the next chunk.
    df_leftover = df[readCopyNumber == 1]  
    # But only if they are close to the end of the chunk (i.e. MAX_FRAG_LEN before the last read in chunk)
    df_leftover = df_leftover[df_leftover.start > ( lastPos - MAX_FRAG_LEN)] 

    df = df[readCopyNumber == 2]  # keep read pairs (read ID observed two times)
    nNotPair = n-nSupp-nNotProper-len(df)

    
    # merge pairs and count fragments
    # for each pair. start = min(start), end = max(end)
    df_pairs = df.groupby("read").agg(
        start=("start", "min"),
        end=("end", "max")
    ).groupby(["start","end"]).size().reset_index(name='Count').astype({"end": int})

    logging.debug(f'reading {n} entries. Start positions {startPos}-{lastPos}, reads: {n}, filtered: {nSupp}(supp), {nNotProper}(not proper pair), {nNotPair}(not pair), {len(df_leftover)}(left over)  fragments: {len(df_pairs)}')

    return( (df_pairs, df_leftover) )

def countFragsContig(f, contig, outFileName):
  outFile = open(outFileName, "w")
  
  my_iter = f.fetch(contig)
  gen = ((x.query_name,x.reference_start, x.reference_end, x.flag ) for x in my_iter)
  
  CHUNK_SIZE = 100000
  dfLeftover = None
  for chunk_gen in grouper_it(CHUNK_SIZE, gen):
      # read chunk
      df = pd.DataFrame(chunk_gen, columns=['read','start','end','flag'])

      # add leftovers from previous iteration
      if dfLeftover is not None:
          df = dfLeftover.append(df)
  
      # process chunk
      (df_pairs, dfLeftover) = countFragmentsChunk(df)
      
      # TODO: the leftover feature may contain reads that are part of fragment which has been counted
      # causing some fragments to be written to file several times
      
      # save chunk results to file
      df_pairs.to_csv(outFile,sep='\t',index=False,header=False)
  
  if dfLeftover is None:
    logging.warning(f'No mapped reads in contig "{contig}"!')
    outFile.close()
    return()
    
  
  logging.debug(f'Final chunk of contig "{contig}"...')
  df_pairs.to_csv(outFile,sep='\t',index=False,header=False)
  outFile.close()
  logging.debug(f'Contig "{contig}" finished!')

def countFragsFile(bamfile, outdir):
  # open bamfile
  logging.info("Reading bam file: "+bamfile)
  f = pysam.AlignmentFile(bamfile,'rb')
  
  # and process each contig separately
  for contig, contigLen in zip(f.references,f.lengths):
    # if( contig.isnumeric()):
    #   chrNr = contig
    # elif( contig.startswith("NC_0273") ):
    #   # Hack: Convert Ssal ICSASG_v2 genome refseq ID to number
    #   chrNr = str(int(contig[7:9])+1)
    # else:
    #   continue #not a chromosome so skip
    #   
    # outFileName = f'{outdir}/{chrNr}.tsv'
    # logging.info(f'Counting fragments in chromosome {chrNr} (seqName: "{contig}") (size: {contigLen}). Output file: {outFileName}')

    # just run for all contigs (chromosome or not)
    outFileName = f'{outdir}/{contig}.tsv'
    logging.info(f'Counting fragments in seqName: "{contig}" (size: {contigLen}). Output file: {outFileName}')
    
    countFragsContig(f, contig, outFileName)
  f.close()

def main():
  logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s', level=logging.DEBUG)
  
  parser = argparse.ArgumentParser(prog='fragmentCount')
  parser.add_argument('bamfile', help='Input bam file, must be sorted by position.')
  parser.add_argument('outdir', help='Directory where results are stored.')
  
  args = parser.parse_args()
  
  if not os.path.isfile(args.bamfile):
    sys.exit(f'Error: File ({args.bamfile}) does not exist ')

  if not os.path.isdir(args.outdir):
    logging.info("Creating directory: "+args.outdir)
    os.mkdir(args.outdir)
  else:
    logging.info(f'Directory ({args.outdir}) already exists. Files will be overwritten.')

  countFragsFile(args.bamfile, args.outdir)
  logging.info('Done!')

if __name__ == '__main__':
  main()
