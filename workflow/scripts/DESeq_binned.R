args = commandArgs(trailingOnly=TRUE)
if (length(args)!=4) 
  stop("Four arguments required!\nUsage: Rscript DESeq_binned.R {input} {output} {nBins} {threads}")



inputDir <- args[1]  
outputFile <- args[2]
nBins <- as.integer(args[3])
threads <- as.integer(args[4])

cat("inputDir =",inputDir,"\n")
cat("outputFile =",outputFile,"\n")
cat("nBins =",nBins,"\n")
cat("threads =",threads,"\n")

suppressPackageStartupMessages({
  library(tidyverse)
  library(DESeq2)
  library(BiocParallel)
})

filterLowCounts <- function(cnt){
  m <- as.matrix(select(cnt, -start, -end))
  filterPass <- rowSums(m==0)==0 & rowMeans(m)>5
  cnt[filterPass, ]
}

binQuantiles <- function(x, nBins){
  cut(x,breaks = unique(quantile(x,probs = seq(0,1,length.out = nBins+1))), include.lowest = T)  
}

cat("read all counts on chromosomes..\n")
cnts <- 
  lapply(setNames(1:29,1:29), function(contig) readRDS(file.path(inputDir,paste0(contig,".RDS"))))
  
  
cat("Filtering fragments with low counts..\n")

cntsFilt <- lapply(cnts,filterLowCounts)
cat(sum(sapply(cntsFilt,nrow)),"of",sum(sapply(cnts,nrow)),"fragments passed filtering.\n")
rm(cnts)


cat("Splitting into similar sized bins based on fragment length..\n")
cntsBinned <-
  cntsFilt %>% 
  bind_rows(.id="chr") %>% 
  split(.,binQuantiles(.$end-.$start,nBins))

rm(cntsFilt)

cat("Number of bins:",length(cntsBinned),"\n")

res <- list()

for( bin in names(cntsBinned)){
  cat("\nRunning DESeq for bin",bin,"\n")
  cnt <- cntsBinned[[bin]]
  cat("generating count matrix\n")
  m <- as.matrix(select(cnt, -chr, -start, -end))
  rownames(m) <- paste0(cnt$chr,":",cnt$start,"-",cnt$end)
  
  cat("DESeqDataSetFromMatrix\n")
  
  dds <- DESeqDataSetFromMatrix(m,tibble(assay = as.factor(sub("_.$","",colnames(m)))), ~assay)
  cat("DESeq\n")
  dds <- DESeq(dds,parallel = T,BPPARAM=MulticoreParam(threads))
  cat("Getting results\n")
  res[[bin]] <- results(dds, name="assay_RNA_vs_DNA")
}

cat("Writing file",outputFile,"\n")
saveRDS(res,outputFile)
cat("Done.\n")
