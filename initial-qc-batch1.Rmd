---
title: "Salmon hidra QC - Library complexity of batch 1"
output: 
  html_document:
    code_folding: hide
editor_options: 
  chunk_output_type: inline
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
options(dplyr.summarise.inform = FALSE)

library(ChIPpeakAnno)
library(GenomicRanges)
library(biomaRt)
library(tidyverse)
library(fuzzyjoin)
```

## fragment counts

```{r load counts}
MergefragCntDirs <- "/mnt/SCRATCH/lagr/salmon-hidra-batch1-prelim/fragment_counts_merged"

# read all counts on chromosome
cnts <- lapply(setNames(1:29,1:29), function(contig) readRDS(file.path(MergefragCntDirs,paste0(contig,".RDS"))))

cnts <- 
  cnts %>% 
  bind_rows(.id="chr") %>% 
  rename( "N"="H01_EKDL210006715-1a_HKH7WDSX2_L1")


```

```{r getTSS, cache=TRUE}
tbl <- getBM(attributes = c("ensembl_gene_id","start_position","end_position","strand","chromosome_name"),
             mart = useDataset(dataset = "ssalar_gene_ensembl", 
                               mart = useMart("ENSEMBL_MART_ENSEMBL")))

tss <-
  tbl %>% 
  mutate(tss=ifelse(strand==1,start_position,end_position)) %>% 
  mutate(strand=ifelse(strand==1,"+","-")) %>% 
  select(chr=chromosome_name,tss,strand)
```


* 47.7 M reads, 42.4 M aligned fragments (89%), 11.1 M unique fragments



```{r}
cnts %>% 
  count(N) %>% 
  mutate(totalFragments = n*N) %>% 
  mutate(N = pmin(N,100)) %>% 
  group_by(N) %>% 
  summarise(totalFragments=sum(totalFragments)) %>% 
  mutate( Nbin = ifelse(N==1,"N=1","N>1")) %>% 
  ggplot(aes(x=N,y=totalFragments,fill=Nbin)) + geom_col(width = 1) + 
  ylab("Total fragments in bin") + xlab("Fragment copies") %>% 
  scale_x_continuous(breaks=c(0,20,40,60,80,100),labels = c("0","20","40","60","80","100+")) +
  scale_fill_manual(values = c("firebrick","dodgerblue"),name="copies")

```

The peak in the distribution at 8 copies indicate the typical copy number, which would give ~6M unique fragments at 48M reads. However there are 6M with only a single copy, which is more than expected. There are 5M unique fragments with 2 or more copies.

Not sure if the single copy fragments represents actual ATAC fragments so will analyse the them separately..

### Fragments Lengths

```{r, fig.width=12, fig.height=3}
cnts %>%
  mutate(len = end-start) %>% 
  mutate( Nbin = ifelse(N==1,"N=1","N>1")) %>% 
  group_by(Nbin) %>% 
  count(len) %>%
  mutate(d=n/sum(n)) %>%
  ungroup() %>% 
  ggplot(aes(x=len,y=d,color=Nbin))+geom_line()+
  ylab("Density")+xlab("Fragment length") +
  scale_color_manual(values = c("firebrick","dodgerblue"),name="copies") +
  scale_x_continuous(breaks=seq(0,450,by=50),minor_breaks =seq(0,450,by=10))
  
```

```{r eval=F,include=F}
cnts %>%
  mutate(len = end-start) %>% 
  mutate( Nbin = ifelse(N==1,"N=1","N>1")) %>% 
  group_by(Nbin) %>% 
  count(len) %>%
  arrange(len) %>% 
  mutate(y=(n-(lead(n,n=5)+lag(n,n=6))/2)/(n+lead(n,n=5))) %>%
  filter(len>40,len < 300) %>% 
  ungroup() %>% 
  ggplot(aes(x=len,y=y,color=Nbin))+geom_line()+
  xlab("Fragment length") +
  scale_color_manual(values = c("firebrick","dodgerblue"),name="copies") +
  scale_x_continuous(breaks=seq(0,450,by=50),minor_breaks =seq(0,450,by=10))

```


```{r}
# given a set of genome positions with orientation and a table of regions 
# count the proportion of overlaps per bin around each position
#
# output: bin, 
# wrapper for ChIPpeakAnno::featureAlignedSignal()
pileupBinsAroundPos <- function(chr,pos,orientation,regionTbl,nbins=200, upstream=5000, downstream=5000){
  
  orientation[is.na(orientation)] <- "*"
  
  # feature.center is a Granges with identical width = 1
  featCenter <- makeGRangesFromDataFrame(tibble(chr,start=pos,end=pos,strand=orientation))
  
  # cvglists ("coverage" lists) is a list of list of Rle.
  # first list is of different classes 
  # second is of chromosomes
  # The Rle is a set of ranges with values 
  cvglists <- 
    regionTbl %>% 
    makeGRangesFromDataFrame(keep.extra.columns = T) %>% 
    split(.$Ncopies) %>%
    lapply(coverage)
  
  # featureAlignedSignal generates a matrix per TEfam with features (i.e. peaks) in rows and bins in columns
  suppressWarnings(
    sig <- ChIPpeakAnno::featureAlignedSignal(cvglists, featCenter, n.tile = nbins,
                                              upstream=upstream, downstream=downstream) 
    
  )
  
  # calculate proportion of TSSs that overlap fragment per bin
  lapply(sig, colMeans, na.rm = T) %>% 
    bind_cols() %>% 
    mutate( bin=head(seq(-upstream,downstream,length.out = nbins+1),nbins)) %>% # bin = position relative to peak summit
    pivot_longer(-bin, names_to = "Ncopies")
  
}

```


### TSS enrichment

```{r}
TSSpileup <- pileupBinsAroundPos(chr = tss$chr, pos = tss$tss, orientation = tss$strand, 
                    regionTbl = mutate(cnts, Ncopies = ifelse(N==1, "N=1","N>1")))


TSSpileup %>% 
  group_by(Ncopies) %>% 
  mutate(y = value/mean(value[abs(bin)>(max(bin)*0.8)])) %>% 
  ungroup() %>% 
  ggplot(aes(x=bin,y=y,color=Ncopies)) + 
  geom_line() + 
  scale_color_manual(values = c("firebrick","dodgerblue"),name="copies") +
  xlab("Distance from TSS") + ylab("Fragment enrichment")

```


### Peak coverage

Using one Liver ATAC-seq peak samples from the aqua-faang salmon bodymap. Specifically the immature female sample, but filtering the peaks that overlap with the consensus in all four conditions (~100k peaks).

#### Enrichment of fragments in peaks

```{r}
peaks <- read_tsv("/mnt/project/Aqua-Faang/seq_results/AtlanticSalmon/BodyMap/ATAC/Liver/results/bwa/mergedReplicate/macs/narrowPeak/AtlanticSalmon_ATAC_Liver_Immature_Female.mRp.clN_peaks.narrowPeak", col_types = "ciicdcdddi",
                     col_names=c("chr_peak","start_peak","end_peak","peakID","score","strand",
                                 "signalValue","pValue","qValue","summit"))

consensus <- read_tsv("/mnt/project/Aqua-Faang/seq_results/AtlanticSalmon/BodyMap/ATAC/Liver/results/bwa/mergedReplicate/macs/narrowPeak/consensus/consensus_peaks.mRp.clN.boolean.txt",col_types = cols(
  .default = col_character(),
  start = col_double(),
  end = col_double(),
  num_peaks = col_double(),
  num_samples = col_double(),
  AtanticSalmon_ATAC_ImmatureFemale_Liver.mRp.clN.bool = col_logical(),
  AtanticSalmon_ATAC_ImmatureMale_Liver.mRp.clN.bool = col_logical(),
  AtanticSalmon_ATAC_MatureFemale_Liver.mRp.clN.bool = col_logical(),
  AtanticSalmon_ATAC_MatureMale_Liver.mRp.clN.bool = col_logical()
))

peaksWithConsensus <-
  consensus %>% 
  filter(if_all(ends_with("bool"))) %>% # filter: all columns ending with "bool" must be true. I.e. peak is in all conditions
  select(chr,start,end) %>% 
  genome_inner_join(peaks,by = c("chr"="chr_peak","start"="start_peak","end"="end_peak"), minoverlap=50) %>% 
  select(-chr,-start,-end)
  

peakSummits <-
  peaksWithConsensus %>% 
  filter(chr_peak %in% unique(cnts$chr)) %>% 
  transmute(chr = chr_peak, summit = start_peak+ summit, strand = "*")

peakPileup <- pileupBinsAroundPos(chr = peakSummits$chr, pos = peakSummits$summit, orientation = peakSummits$strand, 
                  regionTbl = mutate(cnts, Ncopies = ifelse(N==1, "N=1","N>1")))
  

peakPileup %>% 
  group_by(Ncopies) %>% 
  mutate(y = value/mean(value[abs(bin)>(max(bin)*0.8)])) %>% 
  ungroup() %>% 
  ggplot(aes(x=bin,y=y,color=Ncopies)) + 
  geom_line() + 
  scale_color_manual(values = c("firebrick","dodgerblue"),name="copies") +
  xlab("Distance from peak summit") + ylab("Fragment enrichment")

```


#### Number of fragments per peak

```{r}

fragmentsPerPeak <-
  peaksWithConsensus %>% 
  select(chr_peak,start_peak,end_peak) %>% 
  filter(chr_peak %in% unique(cnts$chr)) %>% 
  genome_left_join(filter(cnts,N>1),by = c("chr_peak"="chr","start_peak"="start","end_peak"="end")) %>% 
  group_by(chr_peak,start_peak,end_peak) %>% 
  summarise( len=end_peak[1]-start_peak[1], nUniqFragments=sum(!is.na(N)), meanN = mean(N), .groups="drop")


fragmentsPerPeak %>%
  mutate(nUniqFragments = pmin(15,nUniqFragments)) %>%
  mutate(len = cut(len,breaks = c(0,200,500,1000,Inf))) %>% 
  ggplot(aes(x=nUniqFragments, fill=len)) + geom_bar() +
  xlab("Unique fragments per peak") + ylab("Number of peaks") +
  scale_fill_viridis_d(name="peak length")
```


#### Proportion of fragments that overlap peaks

```{r, fig.width=3, fig.height=5}
proportionOfFragmentsOverlappingPeaks <-
  peaksWithConsensus %>% 
  select(chr_peak,start_peak,end_peak) %>% 
  filter(chr_peak %in% unique(cnts$chr)) %>% 
  genome_right_join(cnts,by = c("chr_peak"="chr","start_peak"="start","end_peak"="end")) %>% 
  mutate( Nbin = ifelse(N==1,"N=1","N>1")) %>% 
  mutate( overlapping = !is.na(chr_peak)) %>% 
  count(Nbin,overlapping)

proportionOfFragmentsOverlappingPeaks %>% 
  ggplot(aes(x=overlapping,y=n,fill=Nbin)) + geom_col(position = "dodge") +
  scale_fill_manual(values = c("firebrick","dodgerblue"),name="copies") +
  ylab("Unique fragments") + xlab("Overlaps with ATAC-seq peak")
```

