# get size of chromosome 21 and 25:
cat /mnt/project/ELIXIR/salmobase/datafiles/genomes/AtlanticSalmon/Ssal_v3.1/sequence_Ensembl/Salmo_salar.Ssal_v3.1.dna_sm.toplevel.fa.gz.fai | grep "^2[15]" | cut -f 1,2 > Ssal_21_25.txt

# Use bedtools to create 200bp window coordinates
module load BEDTools

bedtools makewindows -g Ssal_21_25.txt -w 200 > Ssal_21_25_w200bp.bed

# get fasta (because the bedtools getfasta didn't work on the zipped fasta)
faidx /mnt/project/ELIXIR/salmobase/datafiles/genomes/AtlanticSalmon/Ssal_v3.1/sequence_Ensembl/Salmo_salar.Ssal_v3.1.dna_sm.toplevel.fa.gz 21 25 > Ssal_21_25.fa

# get sequence for each window
bedtools getfasta -fi Ssal_21_25.fa -bed Ssal_21_25_w200bp.bed -tab > Ssal_21_25_w200bp_seq.tsv