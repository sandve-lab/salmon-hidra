#!/bin/bash

module load SRA-Toolkit

OUTDIR="sra_data/rawdata/GSE104001_hidra"
mkdir $OUTDIR

SRA_ID_FILE="sra_data/SRR_Acc_List_GSE104001_hidra.csv"

while IFS= read -r id; do
  echo "cmd: prefetch $id -O $OUTDIR"
  prefetch $id -O $OUTDIR
  fasterq-dump $id --split-files --include-technical -O $OUTDIR
  pigz --no-name --processes 6 $OUTDIR/*.fastq
done < "$SRA_ID_FILE"
